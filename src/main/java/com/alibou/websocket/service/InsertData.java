package com.alibou.websocket.service;

import com.alibou.websocket.chat.ChatMessage;
import com.alibou.websocket.chat.MessageType;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class InsertData implements CommandLineRunner {

    @Autowired
    private NotificationService notificationService;

    @Override
    public void run(String... args) throws Exception {
        insertData();
    }
    int num = 0;
    @SneakyThrows
    public void insertData() {

        ChatMessage chatMessage = new ChatMessage();
        Thread.sleep(10000);
        for (int j = 0; j < 5; j++) {
            Thread.sleep(10000);
            for (int i = 0; i < 10; i++) {
                Thread.sleep(10000);
                chatMessage.setContent("hello from server side! " + num);
                chatMessage.setSender("Notification From Server!");
                chatMessage.setType(MessageType.CHAT);
                notificationService.sendChatMessage(chatMessage);
                num++;
            }
        }
    }
}
