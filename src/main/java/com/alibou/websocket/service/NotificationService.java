package com.alibou.websocket.service;

import com.alibou.websocket.chat.ChatMessage;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

    private final SimpMessagingTemplate messagingTemplate;

    // Inject SimpMessagingTemplate via constructor
    public NotificationService(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    // Method to send a message to all subscribers of "/topic/public"
    public void sendChatMessage(ChatMessage chatMessage) {
        // Send the chat message to all subscribers of "/topic/public"
        messagingTemplate.convertAndSend("/topic/public", chatMessage);
        System.out.println("success notify the client!");
    }
}
