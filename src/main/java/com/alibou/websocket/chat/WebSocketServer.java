/*
package com.alibou.websocket.chat;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.springframework.stereotype.Component;

@Component
@ServerEndpoint(value="/chat")
public class WebSocketServer {

    // Use a synchronized set to store sessions
    private static final Set<Session> sessions = Collections.synchronizedSet(new HashSet<>());

    @OnOpen
    public void onOpen(Session session) {
        // Add the new session to the set
        sessions.add(session);
        System.out.println("WebSocket opened: " + session.getId());
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        // Handle incoming messages (if needed)
        System.out.println("Message received from " + session.getId() + ": " + message);
    }

    @OnClose
    public void onClose(Session session) {
        // Remove the closed session from the set
        sessions.remove(session);
        System.out.println("WebSocket closed: " + session.getId());
    }

    // Method to send data to all connected clients
    public static void sendDataToAllClients(String data) {
        for (Session session : sessions) {
            if (session.isOpen()) {
                try {
                    session.getBasicRemote().sendText(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
*/
